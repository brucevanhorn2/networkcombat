﻿/*
    This file is part of NetworkCombat.

    NetworkCombat is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    NetworkCombat is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NetworkCombat.  If not, see <http://www.gnu.org/licenses/>.
 */

using UnityEngine;
using System.Collections;
using System;

public class TankController : MonoBehaviour, ICanBeDamaged, INeedBullets, ICanUseFuel {
	public GameObject ShellPrefab;
	public Transform FiringPoint;
	public float fireRate = 0.5F;
	public float TankSpeed = 100.0f;
	public float Fuel = 100;
	public float Ammo = 25;
	public float Health = 100;
	public float FuelBurnRate = 0.1f;
	public Transform GameTransform; //this model is in blender and I'm not a good enough 3D guy to fix the rotation problem that presents.  So use this transform for forward and back movements.
	public AudioClip EngineIdleSound;
	public AudioClip EngineRunningSound;
	public AudioClip TurretSound;
	public AudioClip FireSound;
	public AudioClip HitSound;
	public AudioClip DestroySound;

	public GameObject DeadTankPrefab;

	private Rigidbody _rigidBody;
	private float _nextFire = 0.0F;

	// Use this for initialization
	void Start () {
		_rigidBody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		var moveHorizontal = Input.GetAxis("Horizontal");
		var moveVertical = Input.GetAxis("Vertical");

		if (moveVertical > 0)
		{
			Debug.Log("The tank should move forward");
			if (Fuel > 0)
			{
				ExpendFuel(FuelBurnRate);
				_rigidBody.AddForce(GameTransform.forward * TankSpeed * 100, ForceMode.Force);
				//transform.Translate(GameTransform.forward * TankSpeed * Time.deltaTime);
			}
		}

		if (moveVertical < 0)
		{
			if (Fuel > 0)
			{
				Debug.Log("The tank should move backward");
				ExpendFuel(FuelBurnRate);
				_rigidBody.AddForce(GameTransform.forward * (TankSpeed * -0.8f), ForceMode.Force);
			}
		}

		if (moveHorizontal > 0)
		{
			Debug.Log("The tank should rotate right");
			transform.Rotate(Vector3.forward * Time.deltaTime * (TankSpeed / 4));
		}

		if (moveHorizontal < 0)
		{
			Debug.Log("The tank should rotate left");
			transform.Rotate(Vector3.forward * Time.deltaTime * (TankSpeed/4) * -1);
		}

		if (Input.GetButton("Jump") && Time.time > _nextFire)
		{
			Debug.Log("BANG");
			_nextFire = Time.time + fireRate;
			var shell = Instantiate(ShellPrefab, FiringPoint.position, transform.rotation) as GameObject;
			shell.GetComponent<Rigidbody>().AddForce(GameTransform.forward * 10000);
		}
	}

	public void TakeDamage(float howMuch)
	{
		var newHealth = Health - howMuch;
		if (newHealth > 0)
		{
			//say ouch!
			Health = newHealth;
		}
		else {
			//You're dead!
		}
	}

	public void ExpendBullet()
	{
		if (Ammo > 0)
		{
			Ammo--;
		}
	}

	public void GetBullets(int howMany)
	{
		Ammo += howMany;
	}

	public void ExpendFuel(float howMuch)
	{
		Fuel -= howMuch;
	}

	public void GetFuel(float howMuch)
	{
		Fuel += howMuch;
	}
}
