﻿/*
    This file is part of NetworkCombat.

    NetworkCombat is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    NetworkCombat is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NetworkCombat.  If not, see <http://www.gnu.org/licenses/>.
 */

using UnityEngine;
using System.Collections;

public class ShellController : MonoBehaviour {
	public float DamagePerHitLow = 5.0f;
	public float DamagePerHitHigh = 10.0f;
	public GameObject ExplosionPrefab;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other)
	{
		var whatHit = other.gameObject;
		Debug.Log(other.gameObject.name);
		if (whatHit.tag != "Player")
		{
			var checkInterface = whatHit.GetComponent<ICanBeDamaged>();
			if (checkInterface != null)
			{
				var damageDone = Random.Range(DamagePerHitLow, DamagePerHitHigh);
				checkInterface.TakeDamage(damageDone);
			}
			else 
			{ 
				//it's an inanimate object, so why is it not on fire?
				//TODO:  destroy the shell, play explosion sound, instnatiate a smoking hole
			}
			Instantiate(ExplosionPrefab, transform.position, Quaternion.identity);
			Destroy(gameObject, 0.5f);

		}
	}
}
