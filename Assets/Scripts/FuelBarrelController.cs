﻿/*
    This file is part of NetworkCombat.

    NetworkCombat is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    NetworkCombat is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NetworkCombat.  If not, see <http://www.gnu.org/licenses/>.
 */

using UnityEngine;
using System.Collections;

public class FuelBarrelController : MonoBehaviour {
	public float Fuel;

	void Start()
	{
		Fuel = Random.Range(5.0f, 70.0f);
	}

	void OnCollisionEnter(Collider other)
	{
		if (other.gameObject.tag == "Player")
		{
			var checkInterface = other.GetComponent<ICanUseFuel>();
			if (checkInterface != null)
			{
				checkInterface.GetFuel(Fuel);
				Fuel = 0;

				//TODO:  flash the barrel then make it disappear then have it reappear
				//after a significant time delay.
			}
		}
	}
}
