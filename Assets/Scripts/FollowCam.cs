﻿/*
    This file is part of NetworkCombat.

    NetworkCombat is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    NetworkCombat is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with NetworkCombat.  If not, see <http://www.gnu.org/licenses/>.
 */

using UnityEngine;
using System.Collections;

public class FollowCam : MonoBehaviour
{
	public GameObject Player;
	public Camera MapCamera;

	private Vector3 _offset;
	private Vector3 _wheelDistance;
	// Use this for initialization
	void Start()
	{
		_offset = transform.position - Player.transform.position;
		_wheelDistance = new Vector3(0.0f, 0.5f, 0.5f);
	}

	// Update is called once per frame
	void Update()
	{

	}
	void LateUpdate()
	{

		if (Input.GetKeyUp(KeyCode.M))
		{
			//Toggle the map cam
			//MapCamera.enabled = !MapCamera.enabled;
		}
		var d = Input.GetAxis("Mouse ScrollWheel");
		if (d > 0f)
		{
			_offset = (transform.position - Player.transform.position) - _wheelDistance;
		}
		else if (d < 0f)
		{
			_offset = (transform.position - Player.transform.position) + _wheelDistance;
		}
		transform.position = Player.transform.position + _offset;
	}
}